import {API_URL} from "../config/UrlConfig";
import Doctor from "../Shared/Doctor";


const getDoctors = async (): Promise<Doctor[]> => {
    return fetch(`${API_URL}/api/v1/doctors/speciality/hello`)
        .then((res) => res.json())
}

export {getDoctors};