
import DoctorsCards from "../Components/DoctorCard/DoctorsCards";
import SearchWithFilters from "../Components/SearchWithFilters/SearchWithFilters";


const Doctors = () => {
    return <div>
        <SearchWithFilters/>
        <DoctorsCards/>
    </div>
}

export default Doctors