import React from 'react';
import {BrowserRouter as Router, Link, Route, Routes} from 'react-router-dom';
import Doctors from "../../pages/Doctors";
import NavigationMenu from "../NavigationMenu/NavigationMenu";
import HomePage from "../../pages/HomePage";
import './App.css'
import DiagnosePage from "../../pages/DiagnosePage";

function App() {
  return (

          <Router>
              <NavigationMenu/>
              <Routes>
                  <Route path='/doctors' element={ <Doctors/> }/>
                  <Route path='/diagnosis' element={<DiagnosePage/>}/>
                  <Route path='/' element={<HomePage/>}/>
              </Routes>
          </Router>
  )
}

export default App;
