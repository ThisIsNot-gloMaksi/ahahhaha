import React, {useState} from "react";
import './InputChange.css'

interface InputChangeProps {
    name: string,
    visibleName: string,
    defaultValue?: string,
    isUpdate: boolean,
    type?: string,
    handleValue?: (name: string, value: string) => void
}



const InputChange = (props: InputChangeProps) => {
    const [value, setValue] = useState(props.defaultValue)
    const {name, visibleName} = props
    const handleChange = (event: any) => {
        if (!props.isUpdate) {
            return
        }
        const { name, value } = event.target
        if (props.handleValue !== undefined) {
            props.handleValue(name, value)
        }
        setValue(value)
    }
    const inputLabel = <input
        defaultValue={visibleName}
        name={name}
        type={props.type === undefined ? "text" : props.type}
        value={value}
        onChange = {handleChange}
    />

    return <>
        <label className='input_change'>
            <p>{visibleName}: </p>
            <div>
                {
                    props.isUpdate ?  inputLabel : <p>  {value}</p>
                }
            </div>

        </label>
    </>
}
export default InputChange;