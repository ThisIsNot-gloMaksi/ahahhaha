import {Link} from "react-router-dom";
import './NavigationMenu.css'

function NavigationMenu() {
    return (
        <nav className="navigation-menu">
            <ul className="navigation-menu__list">
                <li className="navigation-menu__item">
                    <Link to="/" className="navigation-menu__link">Главная</Link>
                </li>
                <li className="navigation-menu__item">
                    <Link to="/doctors" className="navigation-menu__link">Специалисты</Link>
                </li>
                <li className="navigation-menu__item">
                    <Link to="/doctors" className="navigation-menu__link">Пациенты</Link>
                </li>
                <li className="navigation-menu__item">
                    <Link to="/diagnosis" className="navigation-menu__link">Болезни</Link>
                </li>
            </ul>
        </nav>
    );
}

export default NavigationMenu