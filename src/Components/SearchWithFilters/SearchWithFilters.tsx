import {useState} from "react";
import './SearchWithFilters.css'


const SearchWithFilters = () => {
    const [searchTerm, setSearchTerm] = useState('');
    const [filterOption, setFilterOption] = useState('');

    const handleSearch = () => {
        // Логика для выполнения поиска с учетом выбранных фильтров
        // ...
    };

    return (
        <div className="search-with-filters">
            <input
                type="text"
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
                placeholder="Введите поисковый запрос"
                className="search-with-filters__input"
            />
            <select
                value={filterOption}
                onChange={(e) => setFilterOption(e.target.value)}
                className="search-with-filters__select"
            >
                <option value="">Все специальности</option>
                <option value="option1">Опция 1</option>
                <option value="option2">Опция 2</option>
                <option value="option3">Опция 3</option>
            </select>
            <select
                value={filterOption}
                onChange={(e) => setFilterOption(e.target.value)}
                className="search-with-filters__select"
            >
                <option value="">Все специальности</option>
                <option value="option1">Опция 1</option>
                <option value="option2">Опция 2</option>
                <option value="option3">Опция 3</option>
            </select>
            <button onClick={handleSearch} className="search-with-filters__button">
                Поиск
            </button>
        </div>
    );
}

export default SearchWithFilters