import React, {Component, useState} from 'react';
import InputChange from "../InputChange/InputChange";
import  './DoctorCard.css'
import Doctor from "../../Shared/Doctor";


interface DoctorCardProps {
    id: Number,
    firstName: string,
    lastName: string,
    middleName?: string
    removeButton?: React.ReactNode,
    className?: string
}

const DoctorCart = (props: DoctorCardProps) => {
    const [isUpdate, setUpdate] = useState(false)
    const defaultDoctor: Doctor = {...props}

    const [doctor, setDoctor] = useState<Doctor>(defaultDoctor)

    const handleDoctor = (name: string, value: string) => {
        setDoctor(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    const onSubmit = () => {
        setUpdate(false)
    }

    return (
        <div className={props.className}>
            <div className='doctor_card'>

                <InputChange
                    name='lastName'
                    visibleName='Фамилия'
                    defaultValue={doctor.lastName}
                    isUpdate={isUpdate}
                    handleValue={handleDoctor}
                />
                <InputChange
                    name='firstName'
                    visibleName='Имя'
                    defaultValue={doctor.firstName}
                    isUpdate={isUpdate}
                    handleValue={handleDoctor}
                />
                <InputChange
                    name='speciality'
                    visibleName='Специальность'
                    type='list'
                    isUpdate={isUpdate}
                    handleValue={handleDoctor}
                />
                <InputChange
                    name='experience'
                    visibleName='Опыт работы(в годах)'
                    defaultValue='0'
                    type='number'
                    isUpdate={isUpdate}
                    handleValue={handleDoctor}
                />

            </div>
            <div>

            </div>
            {
                !isUpdate ? <button onClick={() => setUpdate(!isUpdate)}>
                    редактировать
                </button>
                    :  <button color="success" onClick={onSubmit}>
                        сохранить
                    </button>
            }
            {props.removeButton}
        </div>
    );
}

export default DoctorCart;