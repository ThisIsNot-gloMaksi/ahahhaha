import React, {useCallback, useState} from "react";
import Doctor from "../../Shared/Doctor";
import DoctorCard from "./DoctorCard";
import {getDoctors} from "../../Request/doctor.request";


const DoctorsCards = () => {
    const [doctors, setDoctors] = useState<Doctor[]>([])
    useCallback(async () => {
        const doctorsFromApi = await getDoctors()
        setDoctors(doctorsFromApi)
    }, [])
    const handleCreate = () => {
        const newElement: Doctor = {
            id: 0,
            firstName: 'Имя',
            lastName: 'Фамилия'
        }
        setDoctors(prevState => [...prevState, newElement])
    }

    const handleDelete = (index: number) => {
       setDoctors(prev => {
           const copy = [...prev]
           copy.splice(index, 1)
           return copy
       })
    }
    return <div className='home-page__doctors'>
        <div>
            <button onClick={handleCreate}>
                Создать нового врача
            </button>

            {doctors.map((doctor, index) => (
                <DoctorCard
                    className='doctor-card'
                    key={index}
                    id={doctor.id}
                    firstName={doctor.firstName}
                    lastName={doctor.lastName}
                    removeButton={
                        <button onClick={() => handleDelete(index)}>
                            Удалить
                        </button>
                    }
                />
            ))}
        </div>
        {/*<div className="home-page__doctors">*/}
        {/*    <div className="doctor-card">*/}
        {/*        /!*<img src={doctor1} alt="Доктор 1" className="doctor-card__image" />*!/*/}
        {/*        <h2 className="doctor-card__name">Доктор Иванова</h2>*/}
        {/*        <p className="doctor-card__specialty">Терапевт</p>*/}
        {/*    </div>*/}
        {/*</div>*/}

    </div>
}

export default DoctorsCards