interface Doctor {
    id: Number,
    lastName: string,
    firstName: string
}

export default Doctor